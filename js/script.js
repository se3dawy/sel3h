$(document).ready( function() {
 // owl slider
 if( $('.owl-carousel').length > 0 ){
  $('#ad-featured_owl').owlCarousel({
   rtl: true,
   loop: true,
   smartSpeed: 8500,
   margin: 20,
   nav: true,
   dots: false,
   navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
   autoplay: true,
   autoplayTimeout: 3000,
   autoplayHoverPause: true,
   responsive: {
    0: {
     items: 1
    },
    600: {
     items: 1
    },
    1000: {
     items: 4
    }
   }
  });
 }

 if( $('.owl-carousel').length > 0 ){
  $('#wanted_owl').owlCarousel({
   rtl: true,
   loop: true,
   smartSpeed: 8500,
   margin: 20,
   nav: true,
   dots: false,
   navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
   autoplay: true,
   autoplayTimeout: 3000,
   autoplayHoverPause: true,
   responsive: {
    0: {
     items: 1
    },
    600: {
     items: 1
    },
    1000: {
     items: 4
    }
   }
  });
 }
 //---------------------------
 // single horsintal slider
 //---------------------------

 var clickEvent = false;
 $('#myCarouselProducts').carousel({
  interval:   4000
 }).on('click', '.list-group li', function() {
  clickEvent = true;
  $('.list-group li').removeClass('active');
  $(this).addClass('active');
 }).on('slid.bs.carousel', function(e) {
  if(!clickEvent) {
   var count = $('.list-group').children().length -1;
   var current = $('.list-group li.active');
   current.removeClass('active').next().addClass('active');
   var id = parseInt(current.data('slide-to'));
   if(count == id) {
    $('.list-group li').first().addClass('active');
   }
  }
  clickEvent = false;
 });
 //---------------------------
 // Input sponerr
 //---------------------------
 var action;
 $(".number-spinner button").mousedown(function () {
  btn = $(this);
  input = btn.closest('.number-spinner').find('input');
  btn.closest('.number-spinner').find('button').prop("disabled", false);

  if (btn.attr('data-dir') == 'up') {
   action = setInterval(function(){
    if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
     input.val(parseInt(input.val())+1);
    }else{
     btn.prop("disabled", true);
     clearInterval(action);
    }
   }, 50);
  } else {
   action = setInterval(function(){
    if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
     input.val(parseInt(input.val())-1);
    }else{
     btn.prop("disabled", true);
     clearInterval(action);
    }
   }, 50);
  }
 }).mouseup(function(){
  clearInterval(action);
 });


 //---------------------------
 // mega menu in bootsratp
 //---------------------------
 $(".dropdown").hover(
     function() {
      $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
      $(this).toggleClass('open');
     },
     function() {
      $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
      $(this).toggleClass('open');
     }
 );
 //-------------------------
 // tabs with load page
 //-------------------------
 if($('#myTabs a').length > 0){
  $('#myTabs a').click(function (e) {
   location.reload();
  });

  var href = window.location.hash;
  // console.log(href);
  if(href.length > 0){
   var nameId = window.location.hash.substring(1);
   $('#myTabs li').removeClass('active');
   var liTabActive = $('#myTabs li').find("a[href='"+ href +"']");
   // console.log( liTabActive );
   liTabActive.closest('li').addClass('active');
   $('#myTabsContent > .tab-pane').removeClass('active');
   $('#myTabsContent').find('#'+nameId).addClass('active');
  }else{
   $('#myTabs li:first-child').addClass('active');
   $('#myTabsContent .tab-pane:first-child').addClass('active');

  }
 }


// end
});




$(window).load(function() {
 var boxheight = $('#myCarouselProducts .carousel-inner').innerHeight();
 var itemlength = $('#myCarouselProducts .item').length;
 var triggerheight = Math.round(boxheight/itemlength+1);
 $('#myCarouselProducts .list-group-item').outerHeight(triggerheight);
});